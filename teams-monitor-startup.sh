#!/bin/bash
#Start Teams monitor only if it is not running
if ! ps -ef | grep -v grep | grep "teams-monitor.sh" > /dev/null ; then
  cd ~/git/tinhat/teams-monitor
  export $(grep -v '^#' ~/git/tinhat/teams-monitor/.env | xargs)
  ./teams-monitor.sh >presence.log 2>&1 &
  echo "Presence monitor started"
else
  echo "Presence monitor already running"
fi

#if [ “$(ps -ef | grep -v grep | grep teams-monitor | wc -l)” -le "0" ]
#then
#  cd ~/git/tinhat/teams-monitor
#  export $(grep -v '^#' ~/git/tinhat/teams-monitor/.env | xargs)
#  ./teams-monitor.sh >presence.log 2>&1 &
#  echo "Presence monitor started"
#else
# echo "Presence monitor already running"
#fi
