#!/bin/bash
#
# Simple script to publish presence to a MQTT topic
# Command line startup
#   ./teams-monitor.sh >file.log 2>&1 &
#
# REQUIRED VARIABLES
#   MQTT_SERVER - default server
#   MQTT_TOPIC - default topic to send status
#   TEAMS_MONITOR_DELAY - delay in monitoring checks
#
[[ -z "${MQTT_SERVER}" ]] && MQTT_SERVER='127.0.0.1' || MQTT_SERVER="${MQTT_SERVER}"
[[ -z "${MQTT_TOPIC}" ]] && MQTT_TOPIC='home/callpresence/status' || MQTT_TOPIC="${MQTT_TOPIC}"
[[ -z "${TEAMS_MONITOR_DELAY}" ]] && TEAMS_MONITOR_DELAY=2 || TEAMS_MONITOR_DELAY="${TEAMS_MONITOR_DELAY}"

OS_TYPE=$(uname)
MQTT_PUBLISH=$(which mosquitto_pub)
LAST_STATUS="Available"
SPOTIFY_STATUS=$(osascript -e 'tell application "Spotify" to player state')

echo "Presence Monitor - v0.1"
echo ""
echo "-----Config Settings-----------"
echo "OS_TYPE.......: $OS_TYPE"
echo "MQTT_SERVER...: $MQTT_SERVER"
echo "MQTT_TOPIC....: $MQTT_TOPIC"
echo "DELAY.........: $TEAMS_MONITOR_DELAY"
echo "MQTT Client...: $MQTT_PUBLISH"
echo ""
#
# loop forever
for (( ; ; ))
do
    # check netstat to see if we have open ports
    if [ "$OS_TYPE" == "Darwin" ]; then
        OPEN_TEAM_PORTS=$(lsof -iUDP | grep Microsoft | grep "IPv4" | grep -v "*:*" | wc -l | awk '{$1=$1};1')
        OPEN_ZOOM_PORTS=$(lsof -iUDP | grep zoom | wc -l | awk '{$1=$1};1')
    else
        GET_TEAM_STATUS_COMMAND="netstat -l | grep ":500" | wc -l | awk '{$1=$1};1'"
        GET_ZOOM_STATUS_COMMAND="lsof -iUDP | grep zoom | wc -l | awk '{$1=$1};1'"
    fi
    # echo "`date` -- TEAM Ports: $OPEN_TEAM_PORTS  ZOOM Ports: $OPEN_ZOOM_PORTS"

    PLATFORM_IN_USE=""
    if [ "$OPEN_TEAM_PORTS" != "0" ];
    then
        TEAMS_STATUS="On-Call"
        PLATFORM_IN_USE="MSTeams"
    else
        TEAMS_STATUS="Available"
    fi

    if [ "$OPEN_ZOOM_PORTS" != "0" ];
    then
        ZOOM_STATUS="On-Call"
        PLATFORM_IN_USE="Zoom"
    else
        ZOOM_STATUS="Available"
    fi

    if [ "$OPEN_TEAM_PORTS" != "0" ] || [ "$OPEN_ZOOM_PORTS" != "0" ];
    then
        OVERALL_STATUS="On-Call"
    else
        OVERALL_STATUS="Available"
    fi

    # echo "`date` -- Status: TEAMS: $TEAMS_STATUS  ZOOM: $ZOOM_STATUS  OVERALL: $OVERALL_STATUS"

    if [ "$OVERALL_STATUS" != "$LAST_STATUS" ];
    then
        if [ "$OVERALL_STATUS" == "Available" ];
        then
            if [ "$SPOTIFY_STATUS" == "playing" ];
            then
                # echo "Spotify: Sending Play"
                osascript -e 'tell application "Spotify" to play'
            # else
            #     echo "Spotify: Leaving in state: $SPOTIFY_STATUS"
            fi
        fi

        if [ "$OVERALL_STATUS" == "On-Call" ];
        then
            SPOTIFY_STATUS=$(osascript -e 'tell application "Spotify" to player state')
            # echo "Spotify: Saving state and pausing: $SPOTIFY_STATUS"
            osascript -e 'tell application "Spotify" to pause'
        fi

        if [ "$PLATFORM_IN_USE" != "" ];
        then
            echo "`date` -- Presence: $OVERALL_STATUS ($PLATFORM_IN_USE) -- Spotify: $SPOTIFY_STATUS"
            $MQTT_PUBLISH -h "$MQTT_SERVER" -t "$MQTT_TOPIC" -m "$OVERALL_STATUS ($PLATFORM_IN_USE)"
        else
            echo "`date` -- Presence: $OVERALL_STATUS -- Spotify: $SPOTIFY_STATUS"
            $MQTT_PUBLISH -h "$MQTT_SERVER" -t "$MQTT_TOPIC" -m "$OVERALL_STATUS"
        fi
    fi

    LAST_STATUS=$OVERALL_STATUS
    sleep $TEAMS_MONITOR_DELAY
done
