# SAMPLE AppleScript commands
osascript -e 'set volume output muted true'
osascript -e 'output muted of (get volume settings)'
osascript -e 'set volume output muted false'
osascript -e 'tell application "Spotify" to play'
osascript -e 'tell application "Spotify" to name of current track as string'
osascript -e 'tell application "Spotify" to player state'
osascript -e 'tell application "Spotify" to artist of current track & " - " & name of current track'